# SOA labs: 2. Introduce Zuul
## Use Zuul as edge service

### Reproduce steps

1. Clone the project
   * `git clone git@gitlab.com:soa-labs/2018-3.git`

2. Build all docker images:
   * `./build-images.sh`

3. Start mysql (and wait for it to start):
   * `docker-compose up -d mysql`

4. Ensure mysql container is up and running:
   * `docker ps`
   * `docker logs <container-id>` 

5. Start other services (and wait for them to run):
   * `docker-compose up -d`
   * `docker ps` 

6. See eureka in web browser:
   * `http://localhost/eureka`

7. Access app1 through zuul:
   * `http://localhost/zuul/app1/greeting`

8. Access app2 through zuul:
   * `http://localhost/zuul/app2/greeting`

9. Scale app1 with 3 instances:
   * `docker-compose scale app1=3`

9. See instances in eureka, there should be 3 instances of app1:
   * `http://localhost/eureka`

9. Access app1 through zuul, zuul should load-balance through the app1 instances:
   * `http://localhost/zuul/app1/greeting`

9. Shutdown containers:
   * `docker-compose down`
