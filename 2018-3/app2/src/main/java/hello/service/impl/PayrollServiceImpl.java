package hello.service.impl;



import hello.model.Application;
import hello.model.Employee;
import hello.persistance.ApplicationRepository;
import hello.persistance.EmployeeRepository;
import hello.service.PayrollService;
import hello.service.StatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class PayrollServiceImpl implements PayrollService {


    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private ApplicationRepository applicationRepository;


    @Autowired
    StatsService statsService;

    PayrollServiceImpl(){

    }

    @Override
    public Employee EmployeeDetailsById(Long id) {
        Employee eTarget=null;

        eTarget =  employeeRepository.findOne(id);

        if(eTarget!=null){
            statsService.EmployeeTotalTimeSpentWorkingInCompanyById(eTarget.id);
            return eTarget;
        }else{
            Employee err = new Employee();
            err.name="/";
            return err;
        }
    }

    @Override
    public List<Employee> AllEmployees() {
        Iterator<Employee> it = employeeRepository.findAll().iterator();
        ArrayList<Employee> tmp = new ArrayList<Employee>();
        while (it.hasNext()){
            tmp.add(it.next());
        }
        return tmp;
    }

    @Override
    public List<Application> AllApplications() {
        Iterator<Application> it = applicationRepository.findAll().iterator();
        ArrayList<Application> tmp = new ArrayList<>();
        while (it.hasNext()){
            tmp.add(it.next());
        }
        return tmp;
    }

    @Override
    public void EnterEmployee(Employee employee) {
        LocalDate now = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");

        Employee e = new Employee();

        e.name=employee.name;
        e.departmentId=employee.departmentId;
        e.age=employee.age;
        e.gender= employee.gender;
        e.dateEmployed=now.format(formatter);
        e.tasksCompleted=0;
        e.minutesWorked=0;
        e.salary=20000;
        e.ratingPerformance=0;
        e.assignedHoursADay=employee.assignedHoursADay;
        e.role=employee.role;

        employeeRepository.save(e);
    }

    @Override
    public String RemoveEmployeeById(Long id) {
        Employee eTarget=null;

        eTarget = employeeRepository.findOne(id);

        if(eTarget==null){
           return  "Employee with id: "+id+" not found";
        }else{
            employeeRepository.delete(id);
            return "Successfully removed employee with id: "+id;
        }
    }


    public String ChangeSalaryById(Long id, String change){
        Employee eTarget=null;
        eTarget =  employeeRepository.findOne(id);

        if(eTarget!=null){
            try{
                int currSalary = eTarget.salary;
                eTarget.salary=eTarget.salary+Integer.parseInt(change);
                return "Salary for employee with id "+id+" change from "+ currSalary+" to "+eTarget.salary;
            }catch (Exception e){
                return "invalid value for change!";
            }
        }else{
            return "Employee with ID: "+id+" not found";
        }
    }


    @Override
    public String ChangeRole(Long id,  String role) {
        Employee eTarget=null;
        eTarget =  employeeRepository.findOne(id);

        if(eTarget!=null){
            if(role.equals("Employee") || role.equals("Manager") || role.equals("Project Manager") ||  role.equals("CEO") ){
                String tmp = eTarget.role;
                eTarget.role=role;
                return "Successfully changed role to employee with id: "+id +" from "+tmp+" to "+eTarget.role;
            }
            else{
                return "role not valid.";
            }

        }else{
            return "Employee with id: "+id+" not found";
        }
    }

    @Override
    public String CheckInOut(Long id,  String checkInOut) {
        //checkInOut value should be like  "10:00/25/04/2018-16:00/25/04/2018"
        Employee eTarget=null;
        eTarget =  employeeRepository.findOne(id);
        if(eTarget!=null){
            String checkIn = checkInOut.split("-")[0];//10:00/25/04/2018
            String checkOut = checkInOut.split("-")[1];//16:00/25/04/2018

            //to print LocalDateTime checkOutL.format(formatter).toString() ->10:00/25/04/2018
            // with formatter  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm/dd/MM/yyyy");
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm/dd/MM/yyyy");
            LocalDateTime checkInL = LocalDateTime.parse(checkIn,formatter);
            LocalDateTime checkOutL = LocalDateTime.parse(checkOut,formatter);

            if(checkInL.isBefore(checkOutL)){
                eTarget.checkInOut.add(checkIn);
                eTarget.checkInOut.add(checkOut);

                statsService.EmployeeTotalTimeSpentWorkingInCompanyById(eTarget.id);
                return "Successfully added \ncheckIn "+checkInL.format(formatter).toString()+
                        "\ncheckOut "+checkOutL.format(formatter).toString()+
                        "\nto employee with id: "+id;
            }else{
                return "invalid checkInOut";
            }
        }else{
            return "Employee with id: "+id+" not found";
        }
    }


}
